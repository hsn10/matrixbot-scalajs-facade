// shadow sbt-scalajs' crossProject and CrossType from Scala.js 0.6.x
// import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

lazy val matrixbot =
  project
  .enablePlugins(ScalaJSPlugin)
  .in(file("."))
  .settings(
    name := "ScalaJS facade for matrix-bot-sdk",
    normalizedName := "matrix-bot",
    organization := "com.filez.sjs",
    startYear := Some(2020),
    version := "0.0.1-SNAPSHOT",
    // crossScalaVersions := Seq("2.11.12", "2.10.7", "2.12.9", "2.13.1"),
    scalaVersion := "2.13.5",
    Test / parallelExecution := false,
    publishMavenStyle := true,
    Test / publishArtifact   := false,
    pomIncludeRepository := { _ => false },
    publishTo := {
      val nexus = "https://oss.sonatype.org/"
      if (isSnapshot.value)
         Some("snapshots" at nexus + "content/repositories/snapshots")
      else
         Some("releases"  at nexus + "service/local/staging/deploy/maven2")
    },

pomExtra := (
  <url>https://gitlab.com/hsn10/getopt</url>
  <licenses>
    <license>
      <name>MIT</name>
      <url>http://www.opensource.org/licenses/mit-license.php</url>
      <distribution>repo</distribution>
    </license>
  </licenses>
  <scm>
    <connection>scm:git:https://gitlab.com/hsn10/getopt.git</connection>
    <developerConnection>scm:git:git@gitlab.com:hsn10/getopt.git</developerConnection>
    <url>git@gitlab.com:hsn10/getopt.git</url>
  </scm>
  <developers>
    <developer>
      <id>hsn10</id>
      <name>Radim Kolar</name>
    </developer>
  </developers>)
  )
  .settings(
    // npmDependencies in Compile += "matrix-bot-sdk" -> "0.5.2"
    // libraryDependencies += "org.scalatest" %%% "scalatest" % "3.0.8" % "test"
  ).settings(
    jsEnv := new org.scalajs.jsenv.nodejs.NodeJSEnv(),
    scalaJSLinkerConfig ~= { _.withOptimizer(false) }
  )
